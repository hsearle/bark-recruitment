variable "vpc" {}

variable "office_cidr_blocks" {}

variable "env_name" {}


resource "aws_security_group" "internal" {
  name = "${var.env_name}-security-group"
  description = "${var.env_name} internal sg"
  vpc_id      = var.vpc.vpc_id

  tags = {
    Terraform   = "true"
    Environment = terraform.workspace
    Name = "${var.env_name}-security-group"
  }
}

resource "aws_security_group_rule" "egress_all_tcp" {
  description = "Traffic to external sources"

  type = "egress"
  from_port = 0
  to_port = 65535
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.internal.id
}

resource "aws_security_group_rule" "egress_all_udp" {
  description = "Traffic to external sources"

  type = "egress"
  from_port = 0
  to_port = 65535
  protocol = "udp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.internal.id
}

resource "aws_security_group_rule" "self_http" {
  description = "HTTP to self"

  type = "ingress"
  self = true
  from_port = 80
  to_port = 80
  protocol = "tcp"
  security_group_id = aws_security_group.internal.id
}

resource "aws_security_group_rule" "self_https" {
  description = "HTTPS to self"

  type = "ingress"
  self = true
  from_port = 443
  to_port = 443
  protocol = "tcp"
  security_group_id = aws_security_group.internal.id
}

resource "aws_security_group_rule" "ip_office_https" {
  description = "HTTPS Office to environment"
  count   = length(var.office_cidr_blocks) == 0 ? 0 : 1

  type = "ingress"
  cidr_blocks = var.office_cidr_blocks
  from_port = 443
  to_port = 443
  protocol = "tcp"
  security_group_id = aws_security_group.internal.id
}

resource "aws_security_group_rule" "ip_office_ssh" {
  description = "SSH Office to environment"
  count   = length(var.office_cidr_blocks) == 0 ? 0 : 1

  type = "ingress"
  cidr_blocks = var.office_cidr_blocks
  from_port = 22
  to_port = 22
  protocol = "tcp"
  security_group_id = aws_security_group.internal.id
}

resource "aws_security_group_rule" "ip_office_mysql" {
  description = "MySQL Office to environment"
  count   = length(var.office_cidr_blocks) == 0 ? 0 : 1

  type = "ingress"
  cidr_blocks = var.office_cidr_blocks
  from_port = 3306
  to_port = 3306
  protocol = "tcp"
  security_group_id = aws_security_group.internal.id
}

resource "aws_security_group" "external" {
  name = "${var.env_name}-security-group-external"
  description = "${var.env_name} external sg"
  vpc_id      = var.vpc.vpc_id

  tags = {
    Terraform   = "true"
    Environment = terraform.workspace
    Name = "${var.env_name}-security-group-external"
  }
}


resource "aws_security_group_rule" "http_all" {
  description = "HTTP from all"

  type = "ingress"
  from_port = 80
  to_port = 80
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.external.id
}

resource "aws_security_group_rule" "https_all" {
  description = "HTTPS from all"

  type = "ingress"
  from_port = 443
  to_port = 443
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.external.id
}

resource "aws_security_group_rule" "mysql_all" {
  description = "MySQL from all"

  type = "ingress"
  from_port = 3306
  to_port = 3306
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.external.id
}

resource "aws_security_group_rule" "ssh_all" {
  description = "SSH from all"

  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.external.id
}


output "security_groups" {
  value =  {
    internal = aws_security_group.internal.id
    external = aws_security_group.external.id
  }
}
