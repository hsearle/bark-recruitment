#!/bin/bash

git_repo_access_private_key=$1
git_repo_access_public_key=$2
git_repo=$3

wait_file() {
  local file="$1"; shift
  local wait_seconds="${1:-10}"; shift # 10 seconds as default timeout

  until test $((wait_seconds--)) -eq 0 -o -f "$file" ; do sleep 1; done

  ((++wait_seconds))
}

TIME_TO_SLEEP=90
echo "Waiting up to $TIME_TO_SLEEP seconds for user_data to complete"

INSTANCE_READY_FILE=/home/ubuntu/instance_ready
wait_file "$INSTANCE_READY_FILE" "$TIME_TO_SLEEP" || {
  echo "$INSTANCE_READY_FILE still not set after $? seconds"
  exit 1
}

echo "$INSTANCE_READY_FILE file found, continuing"

# start docker
sudo service docker start

# get git repo

export HOME_DIR=/home/ubuntu

sudo echo "$git_repo_access_private_key" > "$HOME_DIR/.ssh/id_rsa"
sudo echo "$git_repo_access_public_key" > "$HOME_DIR/.ssh/id_rsa.pub"

chmod 600 "$HOME_DIR/.ssh/id_rsa"
chmod 600 "$HOME_DIR/.ssh/id_rsa.pub"

if [ -d "$HOME_DIR/src" ]; then sudo rm -Rf "$HOME_DIR/src"; fi

git clone git@bitbucket.org:bark-com/$git_repo.git "$HOME_DIR/src"

cd "$HOME_DIR/src"

# set hostnames for nginx
cd "$HOME_DIR/src/docker/nginx"

export WEB_HOST="$HOSTNAME.r.bark.com"
export API_HOST="$HOSTNAME-api.r.bark.com"

# make dir we need
if [ ! -d "$HOME_DIR/src/docker/nginx/conf.d" ]; then mkdir "$HOME_DIR/src/docker/nginx/conf.d"; fi

# using $$ to escape as we're in a terraform template
envsubst '$${API_HOST}' < templates.conf.d/api.conf > conf.d/api.conf
envsubst '$${WEB_HOST}' < templates.conf.d/web.conf > conf.d/web.conf

cd "$HOME_DIR/src/docker"

docker-compose stop
docker-compose rm -f
docker-compose pull
docker-compose up --build -d

docker-compose exec recruit-api chown -R www-data /var/www
docker-compose exec recruit-api composer install
docker-compose exec recruit-api php artisan key:generate
docker-compose exec recruit-api php artisan config:cache
docker-compose exec recruit-api php artisan migrate --seed

cd "$HOME_DIR/src/web"

npm install
npm run css-compile
npm run build


