variable "git_repo_access_key" {}

locals {
  git_repo = "bark-recruitment-${terraform.workspace}"
}


data "template_file" "startup_script" {
  template = file("${path.module}/scripts/startup-script.sh.tpl")
  vars = {
    git_repo_access_private_key = var.git_repo_access_key.private_key_pem
    git_repo_access_public_key = var.git_repo_access_key.public_key_pem
    git_repo = local.git_repo
  }
}
