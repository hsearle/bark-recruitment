TF_WORKSPACE=$(terraform workspace show)

LOCAL_KEY_DIR=./keys

# if we don't have a key for this user
if [ ! -f "$LOCAL_KEY_DIR/$TF_WORKSPACE" ]
then
  # get all keys from S3
  echo "$LOCAL_KEY_DIR/$TF_WORKSPACE not found, getting keys from S3"
  aws --profile terraformer s3 cp --recursive "s3://bark-recruitment/keys" "$LOCAL_KEY_DIR" >> /dev/null

  # if the file still doesn't exist, create it
  if [ ! -f "$LOCAL_KEY_DIR/$TF_WORKSPACE" ]
  then
    echo "Generating new SSH key for $TF_WORKSPACE"
    ssh-keygen -t rsa -b 2048 -f "keys/$TF_WORKSPACE" -N "" -m PEM
    echo "Done, uploading to S3"
    aws --profile terraformer s3 cp --recursive "$LOCAL_KEY_DIR" "s3://bark-recruitment/keys"
    cat<<EOF

A new SSH key has been generated for $TF_WORKSPACE. This means this is the first time this user has been created. You now need to
  1) Fork the bark-recruitment repository to create bark-recruitment-$TF_WORKSPACE
  2) Add the following SSH Public Key to a user, and give them access to bark-recruitment-$TF_WORKSPACE

EOF
    read  -n 1 -p "When you are ready to continue, press any key" unused_variable
  fi
fi
